package co.simplon.promo16.entity;

import java.time.LocalDate;

public class Dog {
    private Integer id;
    private String names;
    private String breed;
    private LocalDate birthdate;

    



    public Dog(Integer id, String names, String breed, LocalDate birthdate) {
        this.id = id;
        this.names = names;
        this.breed = breed;
        this.birthdate = birthdate;
    }



    public Dog(String names, String breed, LocalDate birthdate) {
        this.names = names;
        this.breed = breed;
        this.birthdate = birthdate;
    }

   

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return names;
    }

    public void setName(String names) {
        this.names = names;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    @Override
    public String toString() {
        return "Dog [birthdate=" + birthdate + ", breed=" + breed + ", id=" + id + ", names=" + names + "]";
    }

}
